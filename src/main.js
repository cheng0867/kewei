import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import './assets/iconfont/iconfont.css'
import store from './store/index'
import axios from "axios"
import VCharts from 'v-charts'
import _ from 'lodash'
// 加载页面
// import indexPage from '@/pages/Index.vue'
// import MapTestPage from '@/pages/MapTest.vue'

Vue.use(ElementUI)
    // Vue.use(VueRouter)
Vue.use(VCharts)
Vue.config.productionTip = false
Vue.prototype.$axios = axios
Vue.prototype._ = _

// const routes = [
//     { path: '/', component: indexPage },
//     { path: '/maptest', component: MapTestPage },
// ]

// const router = new VueRouter({
//     routes
// })

new Vue({
    store,
    router,
    render: h => h(App)
}).$mount('#app')