import Vue from 'vue'
import Vuex from 'vuex'

import map from './modules/map'
import figure from './modules/figure'
import chart from './modules/chart'
import datebar from './modules/datebar'
import timebar from './modules/timebar'
import date from './modules/date'
import menuConfig from '@/utils/menuConfig'
import _ from 'lodash'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        serverConfig: {
            //host: '172.30.11.154',
            // port: '8080'
            // apiString: 'api/',
            host: '172.30.221.156',
            port: '5000',
            apiString: '',
        },
        homeList: menuConfig.homeList,
        // 主菜单
        menu: menuConfig.menu,
        // 子菜单
        subMenu: menuConfig.subMenu,
        // 选项映射
        optionMap: menuConfig.optionMap,
        // 选项集
        options: menuConfig.options,
        region: 'tongzhou',
        pjmenu: 'transport:taxi?action:o?',
        // 请求携带参数
        getOptions: {
            date: [],
            hour: [24, 24],
            scale: 'xz',
            code: 0,
            // 高亮的键值
            key: '',
            name: '',
            // 三级菜单
            groupBtnMenus: [],
            // 四级菜单
            dropMenus: []
        },
        // 选中的菜单按钮组
        btnGroupActive: 1,
        dropGroupActive: ['action', 'o'],
    },
    getters: {
        // 数据接口地址
        apiUrl: (state) => {
            if (state.menu.active > 0) {
                const { host, port, apiString } = state.serverConfig,
                    menu = state.menu.menuList[state.menu.active - 1].url,
                    subMenu = state.subMenu.subMenuList[state.subMenu.active - 1].url,
                    region = state.region

                let mainApiUrl = `http://${host}:${port}/${apiString}${menu}/${subMenu}/${region}/?`
                    // 地图渲染方式
                const mapDrawUrl = state.map.mapDrawUrl
                return mainApiUrl += state.pjmenu + "mapDraw=" + mapDrawUrl + "&"
            }
        },
        curHomeList: state => state.homeList,
        // 主菜单
        curMenu: state => state.menu.menuList,
        // 子菜单
        curSubMenu: state => state.subMenu.subMenuList,
        // 主菜单默认状态
        defaultMenu: state => state.menu.active,
        // 子菜单状态
        curSubMenuActive: state => state.subMenu.active,
        // 映射关系
        curOptionMap: state => state.optionMap,
        // 当前地区
        curRegion: state => state.region,
        // 映射之后的值
        curopOptions: state => {
            //(options) 将两个对象拼接
            let opOptions = Object.assign(state.options.pOptions, state.options.oOptions);
            let menus = '';
            if (state.menu.active == '3' || state.menu.active == '4' || state.menu.active == '5') {
                menus = _.cloneDeep(state.optionMap.special[state.menu.active + '_' + state.subMenu.active + '_' + state.btnGroupActive])
            } else {
                menus = _.cloneDeep(state.optionMap.others[state.menu.active + '_' + state.subMenu.active])
            }
            // 恢复默认值
            changeMenuOptions(menus)

            function changeMenuOptions(arr) {
                for (var i in arr) {
                    if (typeof arr[i] == 'string') {
                        arr[i] = {
                            [arr[i]]: opOptions[arr[i]]
                        }
                    } else {
                        changeMenuOptions(arr[i])
                    }
                }
            }

            return menus
        },
        // 请求的携带参数
        curGetOptions: state => state.getOptions,
        curPjmenu: state => state.pjmenu,
        // 三级菜单
        curBtnGroupActive: state => state.btnGroupActive,
        // 四级菜单
        curDropGroupActive: state => state.dropGroupActive,
    },
    mutations: {
        // 修改主菜单
        CHANGEMENU: (state, payload) => {
            console.log(state.menu.active)
            state.menu.active = payload
        },
        // 修改二级Step菜单
        CHANGESTEPACTIVE: (state, payload) => {
            state.subMenu.active = payload
        },
        CHANGETHIRDMENU: (state, payload) => {
            // 按钮组菜单键值
            state.getOptions.groupBtnMenus.push(payload);
            if (state.getOptions.groupBtnMenus.length > 1) {
                state.getOptions.groupBtnMenus.shift();
            }
            // console.log(state.getOptions.groupBtnMenus)
            let opOptions = Object.assign(state.options.pOptions, state.options.oOptions);
            // 按钮组的样式显示
            opOptions[payload.key].active = payload.active
                // 按钮组点击的值 console.log(opOptions[payload.key].selected)
            opOptions[payload.key].selected[1] = opOptions[payload.key]['option'][payload.active - 1].url
                // 监听的按钮（映射需要）
            state.btnGroupActive = payload.active
        },
        // option选中集合
        CHANGEOPTIONACTIVE: (state, payload) => {
            state.getOptions.dropMenus.push(payload)
            if (state.getOptions.dropMenus.length > 1) {
                state.getOptions.dropMenus.shift()
            }
            // console.log(state.getOptions.dropMenus)
            let opOptions = Object.assign(state.options.pOptions, state.options.oOptions);
            //下拉菜单点击选中的值console.log(opOptions[payload.key].selected)
            opOptions[payload.key].selected[1] = payload.selected[1]
            if (payload.selected[1] == "o" || payload.selected[1] == "d") {
                store.dispatch('map/updateMapDraw', { id: 1, url: 'join' })
            }
            if (payload.selected[1] == 'od' || payload.selected[1] == "track") {
                store.dispatch('map/updateMapDraw', { id: 1, url: 'od' })
            }
            if (payload.selected[1] == "track") {
                store.dispatch('map/updateMapDraw', { id: 1, url: 'track' })
            }
            // 为了控制下面的地图渲染方式
            state.dropGroupActive = payload.selected
        },
        // 更改slidebar上的日期
        CHANGESLDEDATE: (state, payload) => {
            state.getOptions.date.push(payload)
                // 如果是职住模块，就传相同的日期
                // if (state.menu.active == '3' || state.menu.active == '5') {
                //     state.getOptions.date = [];
                //     for (var i = 0; i < 2; i++) {
                //         state.getOptions.date.push(payload)
                //     }
                // } else {
            if (state.getOptions.date.length > 2) {
                state.getOptions.date.shift()
            }
            // }
        },
        CHANGESTARTDATE: (state, payload) => {
            state.startDate = payload
        },
        CHANGEENDDATE: (state, payload) => {
            state.endDate = payload
        },
        // 更改slidebar上的时间
        CHANGESLDETIME: (state, payload) => {
            state.getOptions.hour.push(payload)
            if (state.getOptions.hour.length > 2) {
                state.getOptions.hour.shift()
            }
        },
        CHANGECODE: (state, payload) => {
            state.getOptions.code = payload
        },
        UPDATAOPTIONS: (state, payload) => {
            // 三级四级菜单传给url
            state.pjmenu = payload
        },


        ISSHOWXZLIST: (state, payload) => {
            state.isXZList = payload
        },
        CHANGEBTNACTIVE: (state, payload) => {
            state.btnGroupActive = payload
        },
        CHANGEDROPMENU: (state, payload) => {
            state.dropGroupActive = payload
        },
        CHANGECODEOBJ: (state, payload) => {
            state.getOptions.key = payload.key
            state.getOptions.name = payload.name
        },
        CHANGEREGION: (state, payload) => {
            state.region = payload
        }
    },
    actions: {
        // 修改菜单
        changeMenu: (context, payload) => {
            context.commit('CHANGEMENU', payload)
        },
        changeStepActive: (context, payload) => {
            context.commit('CHANGESTEPACTIVE', payload)
        },
        // 修改菜单按钮组
        changeThirdMenu: (context, payload) => {
            context.commit('CHANGETHIRDMENU', payload)
        },
        // 通州亦庄
        changeRegion: (context, payload) => {
            context.commit('CHANGEREGION', payload)
        },
        // 修改选项
        changeOptionActive: (context, payload) => {
            context.commit('CHANGEOPTIONACTIVE', payload)
        },
        // 日期滑动条 date
        changeSlideDate({ commit }, payload) {
            commit("CHANGESLDEDATE", payload)
        },
        // 日期element的开始日期 不是options 中的date
        changeStartDate({ commit }, payload) {
            commit("CHANGESTARTDATE", payload)
        },
        changeEndDate({ commit }, payload) {
            commit("CHANGEENDDATE", payload)
        },
        // 时间滑动条 hour
        changeSlideTime({ commit }, payload) {
            commit("CHANGESLDETIME", payload)
        },
        changeCode({ commit }, payload) {
            commit("CHANGECODE", payload)
        },
        // 更新三级四级菜单
        updataOPoptions({ commit }, payload) {
            commit("UPDATAOPTIONS", payload)
        },
        isShowXZList({ commit }, payload) {
            commit("ISSHOWXZLIST", payload)
        },
        // 职住的active 默认为1
        changeBtnActive({ commit }, payload) {
            commit("CHANGEBTNACTIVE", payload)
        },
        // 交通的四级菜单默认为 o
        changeDropMenu({ commit }, payload) {
            commit("CHANGEDROPMENU", payload)
        },
        changeCodeObj({ commit }, payload) {
            commit("CHANGECODEOBJ", payload)
        }
    },
    modules: {
        map,
        figure,
        chart,
        datebar,
        timebar,
        date
    }
})

export default store