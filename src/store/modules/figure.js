const state = {
    figureList: [
        // {
        //     title: '流入',
        //     value: 88123,
        //     unit: '人',
        //     children: [{
        //             title: 'A 区域流入',
        //             value: 14738,
        //             unit: '人'
        //         },
        //         {
        //             title: 'B 区域流入',
        //             value: 18237,
        //             unit: '人',
        //         }
        //     ]
        // },
        // {
        //     title: '流入',
        //     value: 88123,
        //     unit: '人',
        //     children: [{
        //             title: 'A 区域流入',
        //             value: 14738,
        //             unit: '人'
        //         },
        //         {
        //             title: 'B 区域流入',
        //             value: 18237,
        //             unit: '人',
        //         }
        //     ]
        // },
        // {
        //     title: '流出',
        //     value: 19208,
        //     unit: '人',
        //     // children:[
        //     //   {
        //     //     title:'A 区域流入',
        //     //     value:14738,
        //     //     unit: '人',
        //     //   },
        //     //   {
        //     //     title:'B 区域流入',
        //     //     value:18237,
        //     //     unit: '人',
        //     //   }
        //     // ]
        // }
    ]
}

const getters = {
    curFigureList: state => state.figureList
}

const mutations = {
    UPDATEFIGURELIST: (state, payload) => {
        state.figureList = payload
    }
}

const actions = {
    updateFigureList({ commit }, payload) {
        commit("UPDATEFIGURELIST", payload)
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}