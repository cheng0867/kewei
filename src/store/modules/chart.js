import _ from 'lodash'
const state = {
    // 数据结构
    chartList: [
        // [{
        //     title: 'XXXX图表1',
        //     type: 'histogram',
        //     data: {
        //         columns: ['日期', '访问用户', '下单用户', '下单率'],
        //         rows: [
        //             { '日期': '1/1', '访问用户': 1393, '下单用户': 1093, '下单率': 0.32 },
        //             { '日期': '1/2', '访问用户': 3530, '下单用户': 3230, '下单率': 0.26 },
        //             { '日期': '1/3', '访问用户': 2923, '下单用户': 2623, '下单率': 0.76 },
        //             { '日期': '1/4', '访问用户': 1723, '下单用户': 1423, '下单率': 0.49 },
        //             { '日期': '1/5', '访问用户': 3792, '下单用户': 3492, '下单率': 0.323 },
        //             { '日期': '1/6', '访问用户': 4593, '下单用户': 4293, '下单率': 0.78 }
        //         ]
        //     }
        // }],
        // [{
        //         title: 'XXXX图表2',
        //         type: 'line',
        //         data: {
        //             columns: ['日期', '访问用户', '下单用户', '下单率'],
        //             rows: [
        //                 { '日期': '1/1', '访问用户': 1393, '下单用户': 1093, '下单率': 0.32 },
        //                 { '日期': '1/2', '访问用户': 3530, '下单用户': 3230, '下单率': 0.26 },
        //                 { '日期': '1/3', '访问用户': 2923, '下单用户': 2623, '下单率': 0.76 },
        //                 { '日期': '1/4', '访问用户': 1723, '下单用户': 1423, '下单率': 0.49 },
        //                 { '日期': '1/5', '访问用户': 3792, '下单用户': 3492, '下单率': 0.323 },
        //                 { '日期': '1/6', '访问用户': 4593, '下单用户': 4293, '下单率': 0.78 }
        //             ]
        //         }
        //     },
        //     {
        //         title: '',
        //         type: 'histogram',
        //         data: {
        //             columns: ['日期', '访问用户', '下单用户', '下单率'],
        //             rows: [
        //                 { '日期': '1/1', '访问用户': 1393, '下单用户': 1093, '下单率': 0.32 },
        //                 { '日期': '1/2', '访问用户': 3530, '下单用户': 3230, '下单率': 0.26 },
        //                 { '日期': '1/3', '访问用户': 2923, '下单用户': 2623, '下单率': 0.76 },
        //                 { '日期': '1/4', '访问用户': 1723, '下单用户': 1423, '下单率': 0.49 },
        //                 { '日期': '1/5', '访问用户': 3792, '下单用户': 3492, '下单率': 0.323 },
        //                 { '日期': '1/6', '访问用户': 4593, '下单用户': 4293, '下单率': 0.78 }
        //             ]
        //         }
        //     }
        // ],
        // [{
        //         title: 'XXXX图表3',
        //         type: 'ring',
        //         data: {
        //             columns: ['日期', '访问用户'],
        //             rows: [
        //                 { '日期': '1/1', '访问用户': 1393 },
        //                 { '日期': '1/2', '访问用户': 3530 },
        //                 { '日期': '1/3', '访问用户': 2923 },
        //                 { '日期': '1/4', '访问用户': 1723 },
        //                 { '日期': '1/5', '访问用户': 3792 },
        //                 { '日期': '1/6', '访问用户': 4593 }
        //             ]
        //         }
        //     },
        //     {
        //         title: '',
        //         type: 'pie',
        //         data: {
        //             columns: ['日期', '访问用户'],
        //             rows: [
        //                 { '日期': '1/1', '访问用户': 1393 },
        //                 { '日期': '1/2', '访问用户': 3530 },
        //                 { '日期': '1/3', '访问用户': 2923 },
        //                 { '日期': '1/4', '访问用户': 1723 },
        //                 { '日期': '1/5', '访问用户': 3792 },
        //                 { '日期': '1/6', '访问用户': 4593 }
        //             ]
        //         }
        //     },
        //     {
        //         title: '',
        //         type: 'doubleLoop',
        //         data: {
        //             columns: ['日期', '访问用户'],
        //             rows: [
        //                 { '日期': '1/1', '访问用户': 1393 },
        //                 { '日期': '1/2', '访问用户': 3530 },
        //                 { '日期': '1/3', '访问用户': 2923 },
        //                 { '日期': '1/4', '访问用户': 1723 },
        //                 { '日期': '1/5', '访问用户': 3792 },
        //                 { '日期': '1/6', '访问用户': 4593 }
        //             ]
        //         }
        //     }
        // ]
    ],
    legendActive: 0,
    chartOption: {
        // colors: ['#77b2ae', '#5397d3', '#e77c8e', '#ddab82', '#b8a7d9', '#749f83', '#ca8622', '#bda29a', '#6e7074', '#546570', '#c4ccd3'],
        colors: [
            '#00ffbf',
            '#FFF5B8',
            '#5397d3',
            '#e77c8e',
            '#ddab82',
            '#b8a7d9',
            '#bda29a',
            '#bf6530',
            '#73ffdc',
            '#074ca6',
            '#31a600',
            '#a63e00'
        ],
        dataZoom: {
            type: 'slider',
            height: 15,
            start: 0,
            end: 100,
            textStyle: {
                color: '#a9adb7'
            },
            bottom: 30
        },
        extends: {
            grid: {
                bottom: 55,
                top: 25,
                right: 20,
                left: 20
            },
            legend: {
                textStyle: {
                    color: '#a9adb7'
                },
                bottom: 8,
                // 饼图图例 滚动属性
                type: 'scroll',
                // 按钮内容设置
                pageIconColor: '#a9adb7',
                pageIconInactiveColor: '#a9adb7',
                pageTextStyle: {
                    color: '#a9adb7'
                },
                // 按钮与图例距离
                pageButtonGap: 15
            },
            xAxis: {
                axisLabel: {
                    color: '#a9adb7',
                    // interval: 1
                },
                axisLine: {
                    show: false
                },
                axisTick: {
                    show: false
                }
            },
            yAxis: {
                axisLabel: {
                    color: '#a9adb7'
                },
                axisLine: {
                    show: false
                },
                splitLine: {
                    lineStyle: {
                        color: '#a9adb7'
                    }
                },
                axisTick: {
                    show: false
                }
            }
        },
        settings: {
            ring: {
                roseType: 'radius',
                radius: [10, 39],
                offsetY: 70
            },
            pie: {
                rules: {
                    // 图表的行位置_列个数
                    pie2_1: {
                        offsetY: '45%',
                        radius: 75
                    },
                    pie2_2: {
                        offsetY: '45%',
                        radius: 75
                    },
                    pie3_1: {
                        offsetY: '38%',
                        radius: 52
                    },
                    pie3_2: {
                        offsetY: '48%',
                        radius: 43,
                        labelLine: {
                            length: 5,
                            length2: 5
                        }
                    },
                    pie3_3: {
                        offsetY: '40%',
                        radius: 50,
                        labelLine: {
                            length: 5,
                            length2: 5
                        }
                    }
                }
            }
        }
    }
}

const getters = {
    curChartList: state => {
        return state.chartList
    },
    curChartOption: state => {
        return state.chartOption
    },
    curName: state => {
        return state.curName
    }
}

const mutations = {
    UPDATECHART(state, payload) {
        state.chartList = payload
    },
    HIGHLIGHT(state, payload) {
        const colors = ["#D42F31", "#FFDC7D", "#FFAB5C", "#F27049", "#D42F31", "#730D1C"];
        // 修改多图表
        state.chartList.map(chart => {
            chart.map(item => {
                if (item.type == 'histogram') {
                    if (!item.data.initRows) {
                        item.data.initRows = _.cloneDeep(item.data.rows)
                    } else {
                        item.data.rows = _.cloneDeep(item.data.initRows)
                    }
                    item.data.rows.map(x => {
                        if (x[payload.key] == payload.name) {
                            // 获取当前除了'地区'的其它字段
                            Object.keys(x).map((n, i) => {
                                if (n != payload.key) {
                                    const val = x[n]
                                    x[n] = {
                                        value: val,
                                        itemStyle: { color: colors[i] }
                                    }
                                }
                            })
                        }
                    })

                }
            })
        })
    }
}

const actions = {
    updatechart({ commit }, payload) {
        commit("UPDATECHART", payload)
    },
    hightLight({ commit }, payload) {
        commit("HIGHLIGHT", payload)
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}