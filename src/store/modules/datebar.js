const state = {
    slideValue: [],
    maxValue: 0,
    // 日期列表
    datebarlist: [],
    sliderDateFormatArr: [],
}

const getters = {
    curDatebarList: state => state.datebarlist,
    curMaxValue: state => state.maxValue,
    curSlideValue: state => state.slideValue,
    sliderDateFormatArr: state => state.sliderDateFormatArr,
}

const mutations = {
    UPDATEDATEBAR: (state, payload) => {
        state.datebarlist = payload
    },
    GETSLIDEVALMAX: (state, payload) => {
        // 绑定最大值
        state.maxValue = payload[1]
            // 绑定默认值
        state.slideValue = payload
    },
    GETSLIDEVALSTART: (state, payload) => {
        state.slideValue.push(payload)
    },
    GETSLIDEVALEND: (state, payload) => {
        state.slideValue.push(payload)
    }
}

const actions = {
    updateDatebar({ commit }, payload) {
        commit("UPDATEDATEBAR", payload)
    },
    getSlideValMax({ commit }, payload) {
        commit("GETSLIDEVALMAX", payload)
    },
    getSlideValStart({ commit }, payload) {
        commit("GETSLIDEVALSTART", payload)
    },
    getSlideValEnd({ commit }, payload) {
        commit("GETSLIDEVALEND", payload)
    },
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}