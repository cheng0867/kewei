// 日期提不出来 否则 点击选择下拉日期选不了
const state = {
    startDate: '',
    endDate: ''
}

const getters = {
    curStartDate: state => state.startDate,
    curEndDate: state => state.endDate
}

const mutations = {
    GETSTARTDATE: (state, payload) => {
        state.startDate = payload
    },
    GETENDDATE: (state, payload) => {
        state.endDate = payload
    }
}

const actions = {
    getStartDate({ commit }, payload) {
        commit("GETSTARTDATE", payload)
    },
    getEndDate({ commit }, payload) {
        commit("GETENDDATE", payload)
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}