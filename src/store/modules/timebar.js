const state = {
    slideValueTime: [24, 24],
    // 是否显示时刻滑动条
    isShowTimeBar: false,
    isShowTimeBtn: true
}

const getters = {
    curSlideValueTime: state => state.slideValueTime,
    curIsShowTimeBar: state => state.isShowTimeBar,
    curIsShowTimeBtn: state => state.isShowTimeBtn
}

const mutations = {
    CHANGESLIDERSTARTTIME: (state, payload) => {
        state.slideValueTime[0] = payload
    },
    CHANGEVALUETIME: (state, payload) => {
        state.slideValueTime = payload
    },
    ISSHOWTIMEBAR: (state, payload) => {
        state.isShowTimeBar = payload
    },
    ISSHOWTIMEBTN: (state, payload) => {
        state.isShowTimeBtn = payload
    }
}

const actions = {
    // 日期反作用给滑动条的方法
    changeSliderStartTime({ commit }, payload) {
        commit("CHANGESLIDERSTARTTIME", payload)
    },
    // 为了重置时刻的方法
    changeValueTime({ commit }, payload) {
        commit("CHANGEVALUETIME", payload)
    },
    isShowTimeBar({ commit }, payload) {
        commit("ISSHOWTIMEBAR", payload)
    },
    isShowTimeBtn({ commit }, payload) {
        commit("ISSHOWTIMEBTN", payload)
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}