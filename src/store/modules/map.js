const state = {
    ops: {
        zoom: 11,
        center: [116.867428, 39.79],
        // 标准（normal）；马卡龙（macaron）；涂鸦（graffiti）；远山黛（whitesmoke）；幻影黑（dark）
        // 草青色（fresh）；极夜蓝（darkblue）；靛青蓝（blue）；月光银（light）；雅士灰（grey）
        mapStyle: 'amap://styles/blue',
        // events: {
        //     init(o, state) {
        //         // getDataByApi({
        //         //     url:"http://172.30.11.154:8080/api/boundary/tongzhou",
        //         //     state:this.state.boundary
        //         // })

        //         // "/api/getTzPg"

        //         // 添加边界
        //         // let marker = new AMap.Marker({
        //         // position: [116.867428, 39.79]
        //         // });
        //         // marker.setMap(o);
        //         console.log('init')
        //         console.log(state)
        //         console.log(state)
        //     }
        //   }
    },
    map: {},
    county: 'tongzhou',
    polycode: '',
    odid: '',
    ptcode: '',
    boundaryData: {},
    mapData: [],
    mapType: ['o', 'join'],
    heatData: [],
    colorData: [],
    ODData: [],
    traceData: [],
    // 渲染的地图集合
    mapDrawList: {
        mapDrawPop: [
            { id: 1, name: '填色', url: 'polygon', icon: 'iconfont icon-jia' }
        ],
        mapDrawOD: [
            { id: 1, name: '出发', url: 'o', icon: 'iconfont icon-jia' },
            { id: 2, name: '到达', url: 'd', icon: 'iconfont icon-jia' }
        ],
        mapDrawTraffic: [
            { id: 1, name: '聚合', url: 'join', icon: 'iconfont icon-jia' },
            { id: 2, name: '热力', url: 'hot', icon: 'iconfont icon-jianshao' },
            { id: 3, name: '填色', url: 'polygon', icon: 'iconfont icon-jianshao' }
        ],
        mapDrawBusSubway: [
            { id: 1, name: '聚合', url: 'join', icon: 'iconfont icon-jia' },
            { id: 2, name: '热力', url: 'hot', icon: 'iconfont icon-jianshao' }
        ],
        mapDrawPlotLand: [
            { id: 1, name: '影像', url: 'raster', icon: 'iconfont icon-jia' },
            { id: 2, name: '填色', url: 'polygon', icon: 'iconfont icon-jianshao' }
        ],
        mapDrawPlotFacity: [
            { id: 1, name: '散点', url: 'scatter', icon: 'iconfont icon-jia' },
            { id: 2, name: '填色', url: 'polygon', icon: 'iconfont icon-jianshao' }
        ]
    },
    //激活地图的渲染状态
    mapDrawActive: 1,
    // 地图传的url参数
    mapDrawUrl: 'join',
    // 地图图例
    mapIsLegend: false,
    // legendData: {
    //     title: '',
    //     items: []
    // },
    mapIsShow: 'true'
}

const getters = {
    curMapData: state => state.mapData,
    curMapDrawList: state => state.mapDrawList,
    curMapDrawActive: state => state.mapDrawActive,
    curMapDrawUrl: state => state.mapDrawUrl,
    curMapIsShow: state => state.mapIsShow,
    curMapIsLegend: state => state.mapIsLegend,
    curOps: state => state.ops,
    // curLegendData: state => state.legendData,
    // 边界数据地址，监听host、区县变化
    // boundaryUrl: (state, getters, rootState) => {
    //     axios.get(`http://${rootState.serverConfig.host}:${rootState.serverConfig.port}/api/boundary/${rootState.county}`).then(res => {
    //         state.boundary = res.data
    //     })
    // },
    curMapType: state => state.mapType,
    // 边界请求地址
    // boundaryUrl: (state, getters, rootState) => {
    //     const host = rootState.serverConfig.host
    //     const port = rootState.serverConfig.port
    //     const county = state.county
    //     let result = []
    //     axios.get(`http://${host}:${port}/api/boundary/${county}`).then(res=>{
    //         result = res.data
    //     })
    //     state.boundaryData = result
    // },

    // 边界数据
    curBoundaryData: state => state.boundaryData,

    // 根据地区拼接边界数据请求地址
    curCounty: (state, getters, rootState) => {
        const host = rootState.serverConfig.host,
            port = rootState.serverConfig.port,
            county = state.county,
            scale = county == 'tongzhou' ? 'xz' : 'taz'

        // return `http://${host}:${port}/api/boundary/${county}`
        return `http://${host}:${port}/boundary/${county}/${scale}`
    }
}

const mutations = {
    UPDATEMAPTYPE: (state, payload) => {
        switch (payload) {
            case "o":
            case "d":
            case "od":
            case "track":
                state.mapType[0] = payload
                break
            case "join":
            case "hot":
            case "polygon":
            case "raster":
            case "scatter":
                state.mapType[1] = payload
        }

    },
    UPDATEMAPDATA: (state, payload) => {
        // 判断对象是否为空
        if (payload !== "") {
            state.mapData = payload
        }
    },
    UPDATEBOUNDARYDATA: (state, payload) => {
        state.boundaryData = payload
    },
    UPDATECOUNTY: (state, payload) => {
        state.county = payload
    },
    UPDATEPOLYCODE: (state, payload) => {
        state.polycode = payload
    },
    UPDATEPTCODE: (state, payload) => {
        state.ptcode = payload
    },
    // 图例
    CHANGEMAPLEGEND: (state, payload) => {
        state.mapIsLegend = payload
    },
    UPDATEMAPDRAW: (state, payload) => {
        state.mapDrawActive = payload.id
        state.mapDrawUrl = payload.url
    },
    UPDATEMAPISSHOW: (state, payload) => {
        state.mapIsShow = payload
    }
}

const actions = {
    updateMapData({ commit }, payload) {
        commit("UPDATEMAPDATA", payload)
    },
    updateMapType({ commit }, payload) {
        commit("UPDATEMAPTYPE", payload)
    },
    updateBoundaryData({ commit }, payload) {
        commit('UPDATEBOUNDARYDATA', payload)
    },
    updateCounty({ commit }, payload) {
        commit('UPDATECOUNTY', payload)
    },
    updatepolycode({ commit }, payload) {
        commit('UPDATEPOLYCODE', payload)
    },
    updateODID({ commit }, payload) {
        commit('UPDATEODID', payload)
    },
    // eslint-disable-next-line no-dupe-keys
    updateODID({ commit }, payload) {
        commit('UPDATEODID', payload)
    },
    updateptCODE({ commit }, payload) {
        commit('UPDATEPTCODE', payload)
    },
    changeMapLegend({ commit }, payload) {
        commit("CHANGEMAPLEGEND", payload)
    },
    // 地图url渲染名称
    updateMapDraw({ commit }, payload) {
        commit("UPDATEMAPDRAW", payload)
    },
    updateMapIsShow({ commit }, payload) {
        commit("UPDATEMAPISSHOW", payload)
    }

}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}