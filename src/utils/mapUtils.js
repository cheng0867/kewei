/* eslint-disable no-redeclare */
/* eslint-disable no-undef */
// var map={},heatlayer={},polygonColors= {},ODLayer= {},traceData= {},dataHeat= {};
import store from '../store/index'
let xz_code_dict = {
    '110112114000': '台湖镇',
    '110112003000': '北苑街道',
    '110112002000': '新华街道',
    '110112004000': '玉桥街道',
    '110112005000': '永顺地区',
    '110112006000': '梨园地区',
    '110112001000': '中仓街道',
    '110112209000': '于家务回族乡',
    '110112109000': '马驹桥镇',
    '110112105000': '张家湾镇',
    '110112117000': '永乐店镇',
    '110112119000': '潞城镇',
    '110112106000': '漷县镇',
    '110112104000': '宋庄镇',
    '110112110000': '西集镇'
}
let taz_code_dict = {
    48: 'Z48',
    60: 'Z60',
    61: 'Z61',
    74: 'Z74',
    757: 'Z757',
    922: 'Z922',
    1220: 'Z1220',
    1382: 'Z1382',
    1453: 'Z1453',
    1590: 'Z1590',
    1635: 'Z1635',
    1690: 'Z1690',
    1751: 'Z1751',
    1760: 'Z1760',
    985: 'Z985',
    1071: 'Z1071',
    47: 'Z47',
    883: 'Z883',
    1836: 'Z1836',
    49: 'Z49',
    75: 'Z75',
    411: 'Z411',
    1070: 'Z1070',
    102: 'Z102',
    209: 'Z209',
    1163: 'Z1163',
    103: 'Z103',
    107: 'Z107',
    892: 'Z892',
    108: 'Z108',
    215: 'Z215',
    731: 'Z731',
    132: 'Z132',
    438: 'Z438',
    1168: 'Z1168',
    133: 'Z133',
    439: 'Z439',
    469: 'Z469',
    134: 'Z134',
    217: 'Z217',
    474: 'Z474',
    484: 'Z484',
    138: 'Z138',
    344: 'Z344',
    139: 'Z139',
    186: 'Z186',
    190: 'Z190',
    1201: 'Z1201',
    219: 'Z219',
    234: 'Z234',
    242: 'Z242',
    252: 'Z252',
    263: 'Z263',
    271: 'Z271',
    272: 'Z272',
    371: 'Z371',
    913: 'Z913',
    289: 'Z289',
    332: 'Z332',
    340: 'Z340',
    342: 'Z342',
    372: 'Z372',
    505: 'Z505',
    557: 'Z557',
    569: 'Z569',
    575: 'Z575',
    580: 'Z580',
    591: 'Z591',
    652: 'Z652',
    603: 'Z603',
    605: 'Z605',
    609: 'Z609',
    612: 'Z612',
    613: 'Z613',
    653: 'Z653',
    736: 'Z736',
    743: 'Z743',
    623: 'Z623',
    634: 'Z634',
    659: 'Z659',
    683: 'Z683',
    703: 'Z703',
    715: 'Z715',
    717: 'Z717',
    730: 'Z730',
    744: 'Z744',
    763: 'Z763',
    769: 'Z769',
    771: 'Z771',
    772: 'Z772',
    780: 'Z780',
    789: 'Z789',
    986: 'Z986',
    793: 'Z793',
    794: 'Z794',
    797: 'Z797',
    800: 'Z800',
    801: 'Z801',
    828: 'Z828',
    880: 'Z880',
    916: 'Z916',
    918: 'Z918',
    923: 'Z923',
    931: 'Z931',
    1048: 'Z1048',
    947: 'Z947',
    962: 'Z962',
    969: 'Z969',
    971: 'Z971',
    973: 'Z973',
    982: 'Z982',
    984: 'Z984',
    1001: 'Z1001',
    1003: 'Z1003',
    1005: 'Z1005',
    1011: 'Z1011',
    1019: 'Z1019',
    1025: 'Z1025',
    1026: 'Z1026',
    1068: 'Z1068',
    1039: 'Z1039',
    1043: 'Z1043',
    1047: 'Z1047',
    1105: 'Z1105',
    1112: 'Z1112',
    1115: 'Z1115',
    1167: 'Z1167',
    1208: 'Z1208',
    1215: 'Z1215',
    1219: 'Z1219',
    1231: 'Z1231',
    1243: 'Z1243',
    1265: 'Z1265',
    1074: 'Z1074',
    1078: 'Z1078',
    1090: 'Z1090',
    1091: 'Z1091',
    1118: 'Z1118',
    1271: 'Z1271',
    1272: 'Z1272',
    1287: 'Z1287',
    1292: 'Z1292',
    1308: 'Z1308',
    1315: 'Z1315',
    1349: 'Z1349',
    1357: 'Z1357',
    1362: 'Z1362',
    1425: 'Z1425',
    1429: 'Z1429',
    1370: 'Z1370',
    1386: 'Z1386',
    1392: 'Z1392',
    1406: 'Z1406',
    1410: 'Z1410',
    1411: 'Z1411',
    1419: 'Z1419',
    1434: 'Z1434',
    1465: 'Z1465',
    1475: 'Z1475',
    1478: 'Z1478',
    1490: 'Z1490',
    1498: 'Z1498',
    1499: 'Z1499',
    1510: 'Z1510',
    1545: 'Z1545',
    1550: 'Z1550',
    1573: 'Z1573',
    1576: 'Z1576',
    1577: 'Z1577',
    1605: 'Z1605',
    1621: 'Z1621',
    1638: 'Z1638',
    1644: 'Z1644',
    1664: 'Z1664',
    1669: 'Z1669',
    1671: 'Z1671',
    1677: 'Z1677',
    1776: 'Z1776',
    1780: 'Z1780',
    1839: 'Z1839',
    1840: 'Z1840',
    1841: 'Z1841',
    1854: 'Z1854',
    1678: 'Z1678',
    1692: 'Z1692',
    1696: 'Z1696',
    1703: 'Z1703',
    1706: 'Z1706',
    1709: 'Z1709',
    1715: 'Z1715',
    1772: 'Z1772',
    1801: 'Z1801',
    1833: 'Z1833',
    1872: 'Z1872',
    1896: 'Z1896',
    1899: 'Z1899',
    1907: 'Z1907',
    1922: 'Z1922',
    1941: 'Z1941',
    1954: 'Z1954',
    1977: 'Z1977'
}
let dk_code_dict = {
        '110100': '110100',
        '110112': '110112',
        '110200': '110200',
        '110205': '110205',
        '110206': '110206',
        '110300': '110300',
        '110301': '110301',
        '110401': '110401',
        '110402': '110402',
        '110403': '110403',
        '110404': '110404',
        '110405': '110405',
        '110406': '110406',
        '110407': '110407',
        '110408': '110408',
        '110409': '110409',
        '110410': '110410',
        '110411': '110411',
        '110412': '110412',
        '110413': '110413',
        '110414': '110414',
        '110415': '110415',
        '110416': '110416',
        '110502': '110502',
        '110503': '110503',
        '110504': '110504',
        '110505': '110505',
        '110506': '110506',
        '110507': '110507',
        '110508': '110508',
        '110509': '110509',
        '110510': '110510',
        '110511': '110511',
        '110512': '110512',
        '110513': '110513',
        '110514': '110514',
        '110515': '110515',
        '110516': '110516',
        '110517': '110517',
        '110518': '110518',
        '110519': '110519',
        '110520': '110520',
        '110521': '110521',
        '110600': '110600',
        '110601': '110601',
        '110701': '110701',
        '110702': '110702',
        '110703': '110703',
        '110704': '110704',
        '110705': '110705',
        '110706': '110706',
        '110707': '110707',
        '110708': '110708',
        '110709': '110709',
        '110710': '110710',
        '110711': '110711',
        '110712': '110712',
        '110713': '110713',
        '110714': '110714',
        '110715': '110715',
        '110716': '110716',
        '110717': '110717',
        '110718': '110718',
        '110719': '110719',
        '110720': '110720',
        '110721': '110721',
        '110722': '110722',
        '110723': '110723',
        '110724': '110724',
        '110725': '110725',
        '110726': '110726',
        '110727': '110727',
        '110728': '110728',
        '110729': '110729',
        '110730': '110730',
        '110801': '110801',
        '110802': '110802',
        '110803': '110803',
        '110804': '110804',
        '110805': '110805',
        '110806': '110806',
        '110807': '110807',
        '110808': '110808',
        '110809': '110809',
        '110901': '110901',
        '110902': '110902',
        '110903': '110903',
        '110904': '110904',
        '110905': '110905',
        '110906': '110906',
        '110907': '110907',
        '110908': '110908',
        '110909': '110909',
        '110910': '110910',
        '110911': '110911',
        '110912': '110912',
        '110913': '110913',
        '110914': '110914',
        '110915': '110915',
        '110916': '110916',
        '110917': '110917',
        '110918': '110918',
        '110919': '110919',
        '110920': '110920',
        '110921': '110921',
        '110922': '110922',
        '110923': '110923',
        '110924': '110924',
        '110925': '110925',
        '110926': '110926',
        '110927': '110927',
        '110928': '110928',
        '110929': '110929',
        '110930': '110930',
        '110931': '110931',
        '110932': '110932',
        '110933': '110933',
        '110934': '110934',
        '110935': '110935',
        '110936': '110936',
        '110937': '110937',
        '110938': '110938',
        '110939': '110939',
        '110940': '110940',
        '110941': '110941',
        '110942': '110942',
        '110943': '110943',
        '110944': '110944',
        '110945': '110945',
        '110946': '110946',
        '110947': '110947',
        '110948': '110948',
        '110949': '110949',
        '111001': '111001',
        '111002': '111002',
        '111003': '111003',
        '111004': '111004',
        '111005': '111005',
        '111006': '111006',
        '111007': '111007',
        '111008': '111008',
        '111009': '111009',
        '111010': '111010',
        '111011': '111011',
        '111012': '111012',
        '111013': '111013',
        '111014': '111014',
        '111015': '111015',
        '111016': '111016',
        '111017': '111017',
        '111018': '111018',
        '111019': '111019',
        '111101': '111101',
        '111102': '111102',
        '111103': '111103',
        '111104': '111104',
        '111105': '111105',
        '111106': '111106',
        '111107': '111107',
        '111108': '111108',
        '111109': '111109',
        '111201': '111201',
        '111202': '111202',
        '111203': '111203',
        '111204': '111204',
        '111205': '111205',
        '111206': '111206',
        '111207': '111207',
        '111208': '111208',
        '111301': '111301',
        '111302': '111302',
        '111303': '111303',
        '111304': '111304',
        '111305': '111305',
        '111401': '111401',
        '111402': '111402',
        '111403': '111403',
        '111404': '111404',
        '111501': '111501',
        '111502': '111502',
        '111503': '111503',
        '111504': '111504',
        '111505': '111505',
        '111506': '111506',
        '111507': '111507',
        '111508': '111508',
        '111509': '111509',
        '111510': '111510',
        '111511': '111511',
        '111512': '111512',
        '111513': '111513',
        '111514': '111514',
        '111515': '111515',
        '111516': '111516',
        '111517': '111517',
        '111518': '111518',
        '111519': '111519',
        '111520': '111520',
        '111521': '111521',
        '111522': '111522',
        '111523': '111523',
    }
    // var colorsRender = [
    //     '#32587F',
    //     '#5C88B6',
    //     '#81A4C4',
    //     '#AABCD4',
    //     '#D1CEE1',
    //     '#3c75bf',
    //     '#5bbf30',
    //     '#bf6530',
    //     '#73ffdc',
    //     '#074ca6',
    //     '#31a600',
    //     '#a63e00'
    // ];


var LabelsData = [
    {text:"新华街道", position:'116.658306457,39.9143858773'},
    {text:"台湖镇",position:'116.60378049,39.8116782061'},
    {text:"永顺地区1",position:'116.654967462,39.9311381643'},
    {text:"永顺地区2",position:'116.717219494,39.882932434'},
    {text:"梨园地区",position:'116.64891311,39.8703546222'},
    {text:"中仓街道",position:'116.672651794,39.9033942759'},
    {text:"北苑街道",position:'116.63284007,39.8994807739'},
    {text:"玉桥街道",position:'116.681769417,39.8848550336'},
    {text:"于家务回族乡",position:'116.712094526,39.6970996914'},
    {text:"马驹桥镇",position:'116.599493335,39.740728433'},
    {text:"张家湾镇",position:'116.706924877,39.8097976414'},
    {text:"永乐店镇",position:'116.790089268,39.6628854395'},
    {text:"潞城镇",position:'116.772200418,39.8734340634'},
    {text:"漷县镇",position:'116.813875697,39.7442799735'},
    {text:"宋庄镇",position:'116.704812508,39.9748196714'},
    {text:"西集镇",position:'116.861122705,39.8008216684'},
]
var colorsRender = [
    // '#4E4B4D',
    '#989797',
    '#E1E1E1',
    '#F9DCCC',
    '#E29071',
    '#A02B30'
];
var landuse_colors = [
    '#6C97A6',
    '#212115',
    '#1904D6',
    '#14FC1C',
    '#F00E12',
    '#9FFCF6',
    '#F01896',
    '#CFB42D',
    '#617CE8',
    '#10A14C',
    '#94506E',
    '#290C59',
    '#3F8EFC',
    '#3F8EFC',
]
// var colors_dict = {
//     'space': ['#B3B3FB', '#FCEFCC', '#E3A7BE'],
//     'time': ['#BDE8FB', '#F5EBDF', '#FEBEBE'],
//     'spaceTime': ['#2892C7', '#82B3A7', '#CADB84', '#FAFA64',  '#FCE158', '#FCCE4E','#FCB644', '#F7722A', '#ED361A']
// }
// var colorsRender = [
//     // '#2F4140',
//     '#315E54',
//     '#408875',
//     '#5EBFAD',
//     '#6DC8B9',
//     '#8AF4F0'
// ];

// import xz_boundary_geojson from './tongzhou_xz'
// import yizhuangBoundary from './yizhuang_taz'


let LabelLayer = null;
// let xz_boundary_layer = null;
export default {
    initMap(paramCenter) {
        var map = new AMap.Map("container", {
            mapStyle: "amap://styles/dark",
            zoom: paramCenter.zoomLev,
            center: paramCenter.centerPt,
            layers: []
        });
        return map;
    },

    // 切换边界（临时）
    changeBoundary(map, GeoJSON){
        // if()
    },

    //设置图层级别可见，在map的zoomend事件中使用
    setZoomsVisable(map, Layer, level1, level2) {
        var zoom = map.getZoom();

        if (zoom > level1 && zoom < level2) {
            Layer.show();
        } else {
            Layer.hide();
        }
    },
    //地图缩放、获取当前等级
    zoomIn(map) { //放大
        map.zoomIn();
    },
    zoomOut(map) { //缩小
        map.zoomOut();
    },
    getZoomLevel(map) { //获取当前缩放级别
        return map.getZoom();
    },

    //地图重绘，保留底图，移除其他图层
    reDrawMap(map, heatlayer, polygonColors, MarkerClusterer, imageLayer, scatterLayer) {
        // debugger
        map.clearInfoWindow();
        let arrLayer = map.getLayers();

        for (let i = 0; i < arrLayer.length; i++) {
            const cLayer = arrLayer[i];
            if (cLayer.CLASS_NAME.indexOf("Custom") != -1) {
                map.remove(cLayer);
            }
        }
        if (heatlayer) {
            map.remove(heatlayer);
        }
        if (polygonColors) {
            map.remove(polygonColors);
        }
        if (MarkerClusterer) {
            map.remove(MarkerClusterer);
        }
        if (imageLayer) {
            map.remove(imageLayer);
        }
        if (scatterLayer) {
            for (let i = 0; i < scatterLayer.length; i++) {
                map.remove(scatterLayer[i])
            }
            this.scatterLayer = []
        }
        if(LabelLayer) {
            map.remove(LabelLayer)
        }

    },

    // 地图重绘
    // reDrawMap(map) {
    //     // 清除地图上的信息窗体
    //     map.clearInfoWindow();

    //     // 获取所有图层
    //     const arrLayer = map.getLayers()
        
    //     console.log(arrLayer)

    //     for (let i = 0; i < arrLayer.length; i++) {
    //         const cLayer = arrLayer[i];
    //         console.log(cLayer)
    //         if (cLayer.CLASS_NAME.indexOf("Custom") != -1) {
    //             map.remove(cLayer);
    //         }
    //     }
    //     // if (heatlayer) {
    //     //     map.remove(heatlayer);
    //     // }
    //     // if (polygonColors) {
    //     //     map.remove(polygonColors);
    //     // }
    //     // if (MarkerClusterer) {
    //     //     map.remove(MarkerClusterer);
    //     // }
    //     // if (imageLayer) {
    //     //     map.remove(imageLayer);
    //     // }
    //     // if (scatterLayer) {
    //     //     for (let i = 0; i < scatterLayer.length; i++) {
    //     //         map.remove(scatterLayer[i])
    //     //     }
    //     //     this.scatterLayer = []
    //     // }
    // },
    // 添加边界数据（暂时注释）
    addPolygonLayer(GeoJson2) {
        // console.log(polygonColors)
        // if (polygonColors) {
        //     map.remove(polygonColors);
        // }
        var geojson = new AMap.GeoJSON({
            geoJSON: GeoJson2,
            // 还可以自定义getMarker和getPolyline
            getPolygon: function(GeoJson2, lnglats) {
                var color2 = "white";

                return new AMap.Polygon({
                    path: lnglats,
                    strokeColor: "white",
                    strokeOpacity: 0.5,
                    fillOpacity: 0.05,
                    fillColor: color2
                });
            }
        });
        // map.setZoomAndCenter(11, [116.727193241678, 39.8021853772893]);
        // geojson.setMap(map);
        return geojson;
    },
    
    //填色图
    addColorPolygon(map, geoJSON, numMax, numMin) {
        let colorValue = geoJSON.classBreaks
        let colors = geoJSON.colorsRender?geoJSON.colorsRender: colorsRender
        // let numArr = geoJSON.classBreaks;
        // if (numMin >= numMax) {
        //     //如果最小值大约等于最大值，返回一个最小值+1
        //     numArr = [numMin + 1];
        // } else { //如果最大值大于最小值，开始分段
        //     let numCha = numMax - numMin;
        //     let numInterval = numCha / 5;
        //     numArr = [numMin + numInterval, numMin + 2 * numInterval, numMin + 3 * numInterval, numMax - numInterval];
        //     // numArr=[10,50,100,150,250];
        // }
        var geojson = new AMap.GeoJSON({
            geoJSON: geoJSON,
            // 还可以自定义getMarker和getPolyline
            getPolygon: function(geojson, lnglats) {
                let color2 = 'red',
                    countC = geojson.properties._parentProperities ?
                    geojson.properties._parentProperities.count :
                    geojson.properties.count
                // console.log(geojson.properties._parentProperities)
                for (let i = 0; i < colorValue.length-1; i++) {
                    if (countC >= colorValue[i] && countC < colorValue[i+1]) {
                        color2 = colors[i]
                        break
                    }else if(countC >= colorValue[colorValue.length-1]){
                        color2 = colors[colorValue.length-1]
                        break
                    }
                }
                return new AMap.Polygon({
                    path: lnglats,
                    strokeColor: "white",
                    strokeOpacity: 0.2,
                    fillOpacity: 0.6,
                    fillColor: color2
                });
                // var color2 = "red";
                // var countC = ''
                // if (geojson.properties._parentProperities) {
                //     countC = geojson.properties._parentProperities.count;
                // } else {
                //     countC = geojson.properties.count;
                // }
                // if (countC) {
                //     if (numArr.length > 1) {
                //         if (countC < numArr[0]) {
                //             color2 = colorsRender[0];
                //         } else if (countC >= numArr[0] && countC < numArr[1]) {
                //             color2 = colorsRender[0];
                //         } else if (countC >= numArr[1] && countC < numArr[2]) {
                //             color2 = colorsRender[1];
                //         } else if (countC >= numArr[2] && countC < numArr[3]) {
                //             color2 = colorsRender[2];
                //         } else if (countC >= numArr[3] && countC < numArr[4]) {
                //             color2 = colorsRender[3];
                //         } else if (countC >= numArr[4] && countC <= numArr[5]) {
                //             color2 = colorsRender[4];
                //         }
                //     } else {
                //         if (countC < numArr[0]) {
                //             color2 = colorsRender[0];
                //         }
                //     }
                // }
                // return new AMap.Polygon({
                //     path: lnglats,
                //     strokeColor: "white",
                //     strokeOpacity: 0.2,
                //     fillOpacity: 0.6,
                //     fillColor: color2
                // });
            }
        });
        //var cThis=this;
        //this.$store.commit('changeMapClickPolyName', 'clickName');

        geojson.on("click", function(e) {
            var obj = e.target;
            var prop = obj.getExtData();
            let polygonOj = new Object();
            // 乡镇code
            if (prop._geoJsonProperties._parentProperities) {
                polygonOj.code = prop._geoJsonProperties._parentProperities.xz_code;
            } else {
                if (prop._geoJsonProperties.xz_code) {
                    polygonOj.code = prop._geoJsonProperties.xz_code
                }
            }
            // taz code
            let tazcode;
            if (prop._geoJsonProperties._parentProperities) {
                tazcode = prop._geoJsonProperties._parentProperities.taz_code
            } else {
                tazcode = prop._geoJsonProperties.taz_code
            }
            let code = '',
                regionName = '',
                codeName = '';
            if (tazcode) {
                code = taz_code_dict[tazcode];
                if (!code) {
                    code = dk_code_dict[tazcode]
                }
                codeName = '';
            } else {
                code = polygonOj.code;
                regionName = xz_code_dict[polygonOj.code];
                codeName = '乡镇';
            }
            // store.dispatch('changeCode', polygonOj);
            // 图表高亮

            store.dispatch('changeCode', code)
            store.dispatch('changeCodeObj', { key: codeName, name: regionName })
                // store.dispatch('chart/hightLight', { key: codeName, name: regionName });
        });
        geojson.setMap(map);
        map.on('zoomend', function() {
            // setZoomsVisable(map,geojson,10,14);
            var zoom = map.getZoom();

            if (zoom >= 14) {
                store.state.getOptions.scale = 'taz';
            } else {
                store.state.getOptions.scale = 'xz';
            }
        });
        // 添加文字标注
        LabelLayer = this.addLabelLayer(map)
        return geojson;
    },
    //添加热图数据
    addHeatMap(map, geoJSON) {
        var heatmap;
        map.plugin(["AMap.Heatmap"], function() {
            //初始化heatmap对象
            heatmap = new AMap.Heatmap(map, {
                radius: 15, //给定半径
                opacity: [0, 0.8]
            });

            var ptArr = [];
            for (var i = 0; i < geoJSON.length; i++) {
                // var lng = flag==0 ? geoJSON[i].o_lng:geoJSON[i].d_lng;
                // var lat = flag==0 ? geoJSON[i].o_lat:geoJSON[i].d_lat;
                var lng = geoJSON[i].lng;
                var lat = geoJSON[i].lat;
                var singleData = {
                    lng: lng,
                    lat: lat
                };
                ptArr.push(singleData);
            }
            //设置数据集
            heatmap.setDataSet({
                data: ptArr,
                max: 1
            });
        });
        
        return heatmap;
    },

    //OD数据直线图
    addODLayer(map, arrOD) {
        if (this.ODLayer) {
            map.remove(this.ODLayer);
        }
          // 添加文字标注
        LabelLayer = this.addLabelLayer(map)
        var ODType = arrOD[0].mapType;
        //var pathSimplifierIns;
        AMapUI.load(["ui/misc/PathSimplifier", "lib/$"], function(PathSimplifier) {
            var maxNum = 15;
            var minNum = 1;
            var pathSimplifierIns = new PathSimplifier({
                zIndex: 100,
                autoSetFitView: false,
                map: map, //所属的地图实例

                getPath: function(pathData) {
                    return pathData.path;
                },
                getHoverTitle: function(pathData, pathIndex, pointIndex) {
                    if (pointIndex >= 0) {
                        //point
                        // pathData.tm +
                        "从" +
                        pathData.oqname + pathData.oname +
                            "到" +
                            pathData.dqname + pathData.dname + ': ' + pathData.count;
                    }

                    return (
                        // pathData.tm +
                        // ":从" +
                        // pathData.oname +
                        // "到" +
                        // pathData.dname
                        "从" +
                        pathData.oqname + pathData.oname +
                        "到" +
                        pathData.dqname + pathData.dname + ':' + pathData.count
                    );
                },
                renderOptions: {
                    // startPointStyle: {
                    //     radius: 4,
                    //     fillStyle: '#109618',
                    //     lineWidth: 1,
                    //     strokeStyle: '#eeeeee'
                    // },
                    // endPointStyle: {
                    //     radius: this.renderOptions.get,
                    //     fillStyle: '#dc3912',
                    //     lineWidth: 1,
                    //     strokeStyle: '#eeeeee'
                    // },
                    pathLineStyle: {
                        dirArrowStyle: true
                    },
                    getPathStyle: function(pathData) {
                        var curPathData = pathData.pathData.count;
                        var curRadius = 0;
                        if(curPathData > 2000){
                            curRadius = 4
                        }else if(curPathData > 1500 && curPathData <= 2000){
                            curRadius = 16
                        }else if(curPathData > 1000 && curPathData <= 1500){
                            curRadius = 14
                        }else if(curPathData > 500 && curPathData <= 1000){
                            curRadius = 12
                        }else if(curPathData > 200 && curPathData <= 500){
                            curRadius = 8
                        }else if(curPathData > 0 && curPathData <= 200){
                            curRadius = 4
                        }
                        var logNum = Math.log(pathData.count);
                        var x = (logNum - Math.log(minNum)) / (Math.log(maxNum) - Math.log(minNum));
                        var GIndex = Math.floor(103 * x + 140);
                        var BIndex = Math.floor(229 * x);
                        var color = 'rgba(255,' + GIndex + ',' + BIndex + ',1)';
                        var lineWidth = x*3;

                        return {
                            startPointStyle: {
                                radius: ODType == 'o'?4: curRadius,
                                fillStyle: '#109618',
                                lineWidth: 1,
                                strokeStyle: '#eeeeee'
                            },
                            endPointStyle: {
                                radius: ODType == 'd'?4: curRadius,
                                fillStyle: '#dc3912',
                                lineWidth: 1,
                                strokeStyle: '#eeeeee'
                            },
                            // keyPointStyle: {
                            //     radius: curRadius,
                            //     fillStyle: '#dc3912',
                            //     lineWidth: 1,
                            //     strokeStyle: '#eeeeee'
                            // },
                            pathLineStyle: {
                                strokeStyle: color,
                                lineWidth: lineWidth
                            },
                            pathLineSelectedStyle: {
                                lineWidth: lineWidth
                            },
                            pathNavigatorStyle: {
                                fillStyle: color
                            }
                        };
                    }
                }
            });

            var flyRoutes = [];
            for (var i = 0, len = arrOD.length; i < len; i++) {

                var singleFly = {
                    // ocode: arrOD[i].o_xz_code,
                    oname: arrOD[i].o_xz_name,
                    oqname: arrOD[i].o_qx_name,
                    dname: arrOD[i].d_xz_name,
                    dqname: arrOD[i].d_qx_name,
                    count: arrOD[i].count,
                    // tm: arrOD[i].date_dt,
                    // dcode: arrOD[i].d_xz_code,
                    // dname: arrOD[i].d_xz_name,
                    path: PathSimplifier.getGeodesicPath(
                        [arrOD[i].o_lng, arrOD[i].o_lat], [arrOD[i].d_lng, arrOD[i].d_lat]
                    )
                };
                flyRoutes.push(singleFly);
            }

            pathSimplifierIns.setData(flyRoutes);
            //点击事件
            pathSimplifierIns.on('pathClick pointClick', function() {
                console.log(store.state.map.odid);
                // store.dispatch("changeCode", info.pathData.ocode);
                console.log(store.state.getOptions.code);
            });


            for (var i = 0, len = flyRoutes.length; i < len; i++) {
                var logNum = Math.log(flyRoutes[i].count);
                var x = (logNum - Math.log(minNum)) / (Math.log(maxNum) - Math.log(minNum));
                var GIndex = Math.floor(103 * x + 140);
                var BIndex = Math.floor(229 * x);
                var color = 'rgba(255,' + GIndex + ',' + BIndex + ',1)';
                var lineWidth = x*1;

                var styleNvg = {
                    loop: true,
                    speed: 10000,
                    pathNavigatorStyle: {
                        width: lineWidth,
                        height: 0.1,
                        //使用图片
                        content: PathSimplifier.Render.Canvas.getImageContent('./imgs/plane.png', onload, onerror),
                        strokeStyle: null,
                        fillStyle: null,
                        //经过路径的样式
                        pathLinePassedStyle: {
                            lineWidth: lineWidth,
                            strokeStyle: color,
                            dirArrowStyle: {
                                stepSpace: 0.5,
                                strokeStyle: color
                            }
                        }
                    }
                };
                var navg = pathSimplifierIns.createPathNavigator(i, styleNvg);
                navg.start();
            }
            
            return pathSimplifierIns;
        });
      
    },

    //添加轨迹
    addPlayMap2(map, traceData) {
        // if (this.imageLayer) {
        //     map.remove(this.imageLayer);
        // }
        // if (this.clusterLayer) {
        //     map.remove(this.clusterLayer);
        // }
        // if (this.heatlayer) {
        //     map.remove(this.heatlayer);
        // }
        // if (this.polygonColors) {
        //     map.remove(this.polygonColors);
        // }
        //使用巡航器的方式加入轨迹
        const _this = this;
        var pathSimplifierIns;
        AMapUI.load(["ui/misc/PathSimplifier", "lib/$"], function(
            PathSimplifier) {
            pathSimplifierIns = new PathSimplifier({
                zIndex: 100,
                autoSetFitView: false,
                map: map,
                getPath: function(pathData) {
                    return pathData.path;
                },
                getHoverTitle: function() {
                    return null;
                },
                renderOptions: {
                    pathLineStyle: {
                        dirArrowStyle: true
                    },
                    getPathStyle: function() {
                        var color = "#ff9900",
                            lineWidth = 1;

                        return {
                            pathLineStyle: {
                                strokeStyle: color,
                                lineWidth: lineWidth
                            },
                            pathLineSelectedStyle: {
                                lineWidth: lineWidth
                            },
                            pathNavigatorStyle: {
                                fillStyle: color
                            }
                        };
                    }
                }
            });

            _this.ODLayer = pathSimplifierIns;

            // var flyRoutes = [{
            //   name:'测试',
            //   path:
            //   traceData
            //     }];
            var flyRoutes = [];
            for (var i = 0; i < traceData.features.length; i++) {
                flyRoutes.push({
                    name: "测试" + (i + 1),
                    path: traceData.features[i].geometry.coordinates
                });
            }
            _this.ODLayer.setData(flyRoutes);
            //点击事件
            //   _this.ODLayer.on("pathClick pointClick", function(e, info) {
            //     console.log(_this.$store.state.clickMapODID);
            //     _this.$store.commit("changeMapClickODID", info.pathData.ID);
            //     console.log(_this.$store.state.clickMapODID);
            //   });

            var styleNvg = {
                loop: true,
                speed: 6000,
                pathNavigatorStyle: {
                    width: 1,
                    height: 2,
                    //使用图片
                    content: PathSimplifier.Render.Canvas.getImageContent(
                        "../assets/circle.png",
                        onload,
                        onerror
                    ),
                    strokeStyle: null,
                    fillStyle: null,
                    //经过路径的样式
                    pathLinePassedStyle: {
                        lineWidth: 2,
                        strokeStyle: "red",
                        dirArrowStyle: {
                            stepSpace: 0.5,
                            strokeStyle: "red"
                        }
                    }
                }
            };

            var navg = _this.ODLayer.createPathNavigator(0, styleNvg);
            navg.start();
        });
    },
    //添加聚合点数据
    addMarkCluster(map, geoJSON) {
        var cluster, markers = [];
        for (var i = 0; i < geoJSON.length; i++) {
            var lng = geoJSON[i].lng;
            var lat = geoJSON[i].lat;
            var mExtData = {
                'id': i,
                // 'dt': geoJSON[i].date_dt,
                // 'xz_code': geoJSON[i].xz_code,
                'info1': geoJSON[i].qx_name || geoJSON[i].info1,
                'info2': geoJSON[i].info2 || '',
                'info3': geoJSON[i].info2 ? '' : geoJSON[i].xz_name,
                // 'qx_code': geoJSON[i].qx_code,
                'count': geoJSON[i].count
            };
            var mk = new AMap.Marker({
                position: new AMap.LngLat(lng, lat),
                extData: mExtData,
                content: '<div style="background-color: hsla(180, 100%, 50%, 0.7); height: 24px; width: 24px; border: 1px solid hsl(180, 100%, 40%); border-radius: 12px; box-shadow: hsl(180, 100%, 50%) 0px 0px 1px;"></div>',
                offset: new AMap.Pixel(-15, -15)
            });
            mk.on('click', function(e) {
                var extdata = e.target.getExtData();
                // store.dispatch('changeCode', extdata.xz_code);
                console.log(store.state.getOptions.code);
                // infoWindow.close();
                var info = [];
                info.push(extdata.info1 + "<br>" + extdata.info2 + extdata.info3 + "：" + extdata.count);
                // info.push("个数：" + extdata.count);
                var infoWindow = new AMap.InfoWindow({
                    isCustom: true,
                    content: info.join("<br/>")
                });
                infoWindow.open(map, e.lnglat);
            });
            markers.push(mk);

        }
        cluster = new AMap.MarkerClusterer(map, markers, {
            gridSize: 80,
            // renderClusterMarker: _renderClusterMarker
        });

        return cluster;
    },
    //添加散点
    addScatterMarker(map, geoJSON) {
        var markers = []
        for (var i = 0; i < geoJSON.length; i += 1) {
            var center = new AMap.LngLat(geoJSON[i].lng, geoJSON[i].lat);
            var circleMarker = new AMap.CircleMarker({
                center: center,
                radius: 5, //3D视图下，CircleMarker半径不要超过64px
                strokeColor: 'white',
                strokeWeight: 2,
                strokeOpacity: 0.5,
                fillColor: 'rgba(0,0,255,1)',
                fillOpacity: 0.5,
                zIndex: 10,
                bubble: true,
                cursor: 'pointer',
                clickable: true
            })
            markers.push(circleMarker)
            circleMarker.setMap(map)
        }
        this.scatterLayer = markers;
        return this.scatterLayer
    },
    //gps坐标转高德坐标，需要输入数组数据
    convert2Amap(lnglats) {
        var reLngLatas = [];
        for (var i = 0; i < lnglats.length; i++) {
            var singleData = lnglats[i];
            if (!isNaN(singleData[0]) && !isNaN(singleData[1])) {
                AMap.convertFrom(singleData, 'gps', function(status, result) {
                    if (result.info === 'ok') {
                        var lnglats1 = result.locations;
                        reLngLatas.push(lnglats1);
                    }
                });
            } else {
                var innerData = [];
                for (var j = 0; j < singleData.length; j++) {
                    var single2LL = singleData[j];
                    if (!isNaN(single2LL[0]) && !isNaN(single2LL[1])) {
                        AMap.convertFrom(single2LL, 'gps', function(status, result2) {
                            if (result2.info === 'ok') {
                                var lnglats2 = result2.locations;
                                innerData.push(lnglats2);
                            }
                        });
                    } else {
                        var innerData2 = [];
                        for (var k = 0; k < single2LL.length; k++) {
                            var single3LL = single2LL[k];
                            if (!isNaN(single3LL[0]) && !isNaN(single3LL[1])) {
                                AMap.convertFrom(single3LL, 'gps', function(status, result3) {
                                    if (result3.info === 'ok') {
                                        var lnglats3 = result3.locations;
                                        innerData2.push(lnglats3);
                                    }
                                });
                            }
                        }
                        innerData.push(innerData2);
                    }
                }
            }
            reLngLatas.push(innerData);
        }
        return reLngLatas;
    },
    // 影像栅格
    addRaster(map, url, bounds) {
        // if (this.imageLayer) {
        //     map.remove(this.imageLayer);
        // }
        // if (this.polygonColors) {
        //     map.remove(this.polygonColors);
        // }
        this.imageLayer = new AMap.ImageLayer({
            url: url,
            bounds: new AMap.Bounds(
                bounds[0], bounds[1]
                // [116.514615, 39.591903], [116.947859, 40.030671]
            ),
            zooms: [3, 18]
        });
        this.imageLayer.setMap(map)
        return this.imageLayer
            // return this.imageLayer
    },
    //海量点添加方法
    addMassMarks(map, geoJSON) {
        var style = [{
            url: 'https://a.amap.com/jsapi_demos/static/images/mass0.png',
            // eslint-disable-next-line no-undef
            anchor: new AMap.Pixel(6, 6),
            size: new AMap.Size(11, 11)
        }, {
            url: 'https://a.amap.com/jsapi_demos/static/images/mass1.png',
            anchor: new AMap.Pixel(4, 4),
            size: new AMap.Size(7, 7)
        }, {
            url: 'https://a.amap.com/jsapi_demos/static/images/mass2.png',
            anchor: new AMap.Pixel(3, 3),
            size: new AMap.Size(5, 5)
        }];

        var citys = [];
        for (var i = 0; i < geoJSON.length; i++) {
            var lng = flag == 0 ? geoJSON[i].o_lng : geoJSON[i].d_lng;
            var lat = flag == 0 ? geoJSON[i].o_lat : geoJSON[i].d_lat;
            citys.push({
                lnglat: [lng, lat],
                name: 'test',
                style: 2
            })
        }
        var mass = new AMap.MassMarks(citys, {
            opacity: 0.8,
            zIndex: 111,
            cursor: 'pointer',
            style: style
        });
        mass.setMap(map);
    },
    addLabelLayer(map) {
        var Labels = []
        for(var i=0;i<LabelsData.length;i++) {
            let position = LabelsData[i].position.split(',')
            Labels.push(
                {
                    name:'通州区',
                    position: [position[0].toString(),position[1].toString()],
                    text: {
                        content: LabelsData[i].text,
                        direction: 'center',
                        style:{
                            // fillColor: "#eee",
                            fontSize: 15,
                            fontWeight: "normal",
                            strokeColor: 'white',//"#c67805",
                            strokeWidth: 2
                        }
                    }
                }
            )
        }
        var layer = new AMap.LabelsLayer({
            rejectMapMask: true,
            // 开启标注避让，默认为开启，v1.4.15 新增属性
            collision: true,
            // 开启标注淡入动画，默认为开启，v1.4.15 新增属性
            animation: true,
        });
        map.add(layer);
        for (var i = 0; i < Labels.length; i++) {
            var labelsMarker = new AMap.LabelMarker(Labels[i]);
            layer.add(labelsMarker);
        }
        return layer;
        
        // layer.setData(xz_coords, {
        //     lnglat: function (obj) {
        //         return obj.value.coord.split(',')
        //     }
        // }).setOptions({
        //     style: {
        //         direction: "right",  // 文字位置
        //         offset: [0, 0],  // 文字偏移距离
        //         zooms: [3, 18],  // 文字显示范围
        //         text: function (data) {
        //             return data.value.value
        //         },  // 文本内容
        //         fillColor: function () {
        //             return `rgb(${Math.random() * 255},${Math.random() * 255},${Math.random() * 255})`
        //         },  // 文字填充色
        //         fontFamily: '字体',  // 文字字体(2D)
        //         fontSize: 30,  // 文字大小, 默认值：12
        //         fontWeight: "normal",  // 文字粗细(2D)。 可选值： 'normal'| 'lighter'| 'bold' 。默认值：'normal'
        //         strokeColor: "rgba(255,255,255,0.85)",  // 文字描边颜色
        //         strokeWidth: 1,  // 文字描边宽度
        //     },
        //     // 鼠标悬停在文字上的样式
        //     selectStyle: {
        //         fillColor: "red",
        //         fontSize: 35,
        //         fontWeight: "normal",
        //         strokeColor: "blue",
        //         strokeWidth: 4
        //     }
        // }).render();
        
        // 监听图层鼠标悬停事件，通过 setOverlaysStyle 方法改变当前文字样式
        // layer.on('mouseover', function (ev) {
        //     console.log(ev.overlay.properties.value);
        //     layer.setOverlaysStyle(function (overlay) {
        //         return overlay._id == ev.overlay._id;
        //     }, {
        //             fillColor: `red`,
        //         });
        // });
        
        // // 监听图层鼠标离开事件，通过 setOverlaysStyle 方法还原当前文字样式
        // layer.on('mouseout', function (ev) {
        //     console.log(ev.overlay.properties.value);
        //     layer.setOverlaysStyle(function (overlay) {
        //         return overlay._id == ev.overlay._id;
        //     }, {
        //             fillColor: `blue`,
        //         });
        // });
    }
}