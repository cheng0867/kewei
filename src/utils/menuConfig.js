export default {
    // 通州和亦庄
    homeList: [{
        name: '通州',
        value: 'tongzhou'
    }, {
        name: '亦庄',
        value: 'yizhuang'
    }],
    // 主菜单
    menu: {
        menuList: [{
                'id': "1",
                title: '区域联系',
                url: 'region'
            },
            {
                'id': "2",
                title: '人口分布',
                url: 'population'
            },
            {
                'id': "3",
                title: '职住特征',
                url: 'career'
            },
            {
                'id': "4",
                title: '交通特征',
                url: 'traffic'
            },
            {
                'id': "5",
                title: '地块功能',
                url: 'block'
            }
        ],
        active: "4"
    },
    // 子菜单
    subMenu: {
        subMenuList: [{
                'id': 1,
                title: '监测',
                icon: 'iconfont icon-iconfonticon-baobia',
                url: 'monitor'
            },
            {
                'id': 2,
                title: '评价',
                icon: 'iconfont icon-pingjia1',
                url: 'comment'
            },
            {
                'id': 3,
                title: '预测',
                icon: 'iconfont icon-xiaoliangyuce',
                url: 'forecast'
            }
        ],
        active: 1
    },

    // 映射关系 假设
    /* 
    键：menu_submenu 
    值：5个option三级菜单 与 ‘o1,o2,o3’键对应
    */
    optionMap: {
        special: {
            '3_1_1': [
                ['p3'],
                ['o4', 'o5']
            ],
            '3_1_2': [
                ['p3'],
                ['o6', 'o7', 'o12']
            ],
            '4_1_1': [
                ['p1'],
                ['o1']
            ],
            '4_1_2': [
                ['p1'],
                ['o11']
            ],
            '4_1_3': [
                ['p1'],
                ['o11']
            ],
            '4_1_4': [
                ['p1'],
                ['o1']
            ],
            '5_1_1': [
                ['p4'],
                ['o8']
            ],
            '5_1_2': [
                ['p4'],
                ['o9']
            ],

        },
        others: {
            '2_1': [
                ['p2'],
                ['o3', 'o2']
            ],
            '2_2': [
                ['p5']
            ]
        }
    },
    // 菜单具体各项
    options: {
        // 按钮组
        pOptions: {
            p1: {
                option: [
                    { id: 1, label: '出租车', url: 'taxi', icon: 'iconfont icon-chuzuche' },
                    { id: 2, label: '地铁', url: 'subway', icon: 'iconfont icon-ditie' },
                    { id: 3, label: '公交', url: 'bus', icon: 'iconfont icon-gongjiaoche' },
                    { id: 4, label: '共享单车', url: 'sharebike', icon: 'iconfont icon-gongxiangdanche' }
                ],
                selected: ['transport', 'taxi'],
                active: 1
            },
            p2: {
                option: [
                    { id: 1, label: '空间分布', url: 'popSpace', icon: 'iconfont icon-chuzuche' },
                    { id: 2, label: '时序变化', url: 'popTimesqure', icon: 'iconfont icon-chuzuche' }
                ],
                // 空间属性
                selected: ['popSpatioMonitor', 'popSpace'],
                active: 1
            },
            p3: {
                option: [
                    { id: 1, label: '职住监测', url: 'occupationMonitor', icon: 'iconfont icon-chuzuche' },
                    { id: 2, label: '通勤监测', url: 'commuterMonitor', icon: 'iconfont icon-ditie' }
                ],
                selected: ['occupationCommuter', 'occupationMonitor'],
                active: 1
            },
            p4: {
                option: [
                    { id: 1, label: '土地利用', url: 'landUse', icon: 'iconfont icon-chuzuche' },
                    { id: 2, label: '设施分布', url: 'facyDistrib', icon: 'iconfont icon-ditie' }
                ],
                selected: ['plotModule', 'landUse'],
                active: 1
            },
            p5: {
                option: [
                    { id: 1, label: '空间分布', url: 'space', icon: 'iconfont icon-chuzuche' },
                    { id: 2, label: '时间变化', url: 'time', icon: 'iconfont icon-ditie' },
                    { id: 3, label: '时空变化', url: 'spaceTime', icon: 'iconfont icon-ditie' }
                ],
                selected: ['popComment', 'space'],
                active: 1
            }
        },
        // 下拉选择
        oOptions: {
            'o1': {
                'option': [{
                    value: 'action',
                    label: '出行模式',
                    children: [{
                            value: 'o',
                            label: '出发'
                        },
                        {
                            value: 'd',
                            label: '到达'
                        },
                        {
                            value: 'od',
                            label: '路径'
                        },
                        {
                            value: 'track',
                            label: '轨迹'
                        }
                    ],
                }],
                default: ['action', 'o'],
                selected: ['action', 'o']
            },
            'o2': {
                'option': [{
                    value: 'popAge',
                    label: '年龄',
                    children: [{
                            value: '8',
                            label: '全部'
                        }, {
                            value: '0',
                            label: '15以下'

                        },
                        {
                            value: '1',
                            label: '15-18'
                        },
                        {
                            value: '2',
                            label: '19-24'
                        },
                        {
                            value: '3',
                            label: '25-34'
                        },
                        {
                            value: '4',
                            label: '35-44'
                        },
                        {
                            value: '5',
                            label: '45-54'
                        },
                        {
                            value: '6',
                            label: '55-64'
                        },
                        {
                            value: '7',
                            label: '65以上'
                        }
                    ]
                }],
                default: ['popAge', '8'],
                selected: ['popAge', '8']
            },
            'o3': {
                'option': [{
                    value: 'popProperty',
                    label: '人口属性',
                    children: [{
                            value: 'popNum',
                            label: '人口数量'

                        },
                        {
                            value: 'popDensity',
                            label: '人口密度'
                        }
                    ]
                }],
                default: ['popProperty', 'popNum'],
                selected: ['popProperty', 'popDensity']
            },
            'o4': {
                'option': [{
                    value: 'occupationProperty',
                    label: '人口属性',
                    children: [{
                            value: 'reside',
                            label: '居住人口'

                        },
                        {
                            value: 'working',
                            label: '就业人口'
                        }
                    ]
                }],
                default: ['occupationProperty', 'reside'],
                selected: ['occupationProperty', 'reside']
            },
            'o5': {
                'option': [{
                    value: 'occupationAge',
                    label: '年龄段',
                    children: [{
                            value: '0',
                            label: '全部年龄'

                        },
                        {
                            value: '1',
                            label: '15岁以下'
                        },
                        {
                            value: '2',
                            label: '15-18岁'
                        },
                        {
                            value: '3',
                            label: '19-24岁'
                        },
                        {
                            value: '4',
                            label: '25-34岁'
                        },
                        {
                            value: '5',
                            label: '35-44岁'
                        },
                        {
                            value: '6',
                            label: '45-54岁'
                        },
                        {
                            value: '7',
                            label: '55-64岁'
                        },
                        {
                            value: '8',
                            label: '65岁以上'
                        }
                    ]
                }],
                default: ['occupationAge', '0'],
                selected: ['occupationAge', '0']
            },
            'o6': {
                'option': [{
                    value: 'commuteWay1',
                    label: '通勤方式1',
                    children: [{
                            value: 'total',
                            label: '通勤总量'

                        },
                        {
                            value: 'crossRegion',
                            label: '跨区通勤'
                        },
                        {
                            value: 'inRegion',
                            label: '区内通勤'
                        }
                    ]
                }],
                default: ['commuteWay1', 'total'],
                selected: ['commuteWay1', 'total']
            },
            'o7': {
                'option': [{
                    value: 'commuteWay2',
                    label: '通勤方式2',
                    children: [{
                        value: 'o',
                        label: '通勤出发'
                    }, {
                        value: 'd',
                        label: '通勤到达'

                    }]
                }],
                default: ['commuteWay2', 'o'],
                selected: ['commuteWay2', 'o']
            },
            'o8': {
                'option': [{
                    value: 'landUseType',
                    label: '用地类型',
                    children: [{
                        value: 'waterField',
                        label: '水田'
                    }, {
                        value: 'dryField',
                        label: '旱地'
                    }, {
                        value: 'youWoodland',
                        label: '有林地'
                    }, {
                        value: 'shuWoodland',
                        label: '疏林地'
                    }, {
                        value: 'otherWoodland',
                        label: '其他林地'
                    }, {
                        value: 'largeCoverageLea',
                        label: '高覆盖度草地'
                    }, {
                        value: 'lowCoverageLea',
                        label: '低覆盖度草地'
                    }, {
                        value: 'waterways',
                        label: '河渠'
                    }, {
                        value: 'reservoir',
                        label: '水库坑塘'
                    }, {
                        value: 'shallows',
                        label: '滩地'
                    }, {
                        value: 'townUseLand',
                        label: '城镇用地'
                    }, {
                        value: 'villageUseLand',
                        label: '农村居民点'
                    }, {
                        value: 'otherUseLand',
                        label: '其它建设用地'
                    }, {
                        value: 'bareLand',
                        label: '裸土地'
                    }]
                }],
                default: ['landUseType', 'waterField'],
                selected: ['landUseType', 'waterField']
            },
            'o9': {
                'option': [{
                    value: 'facilityType',
                    label: '设施类型',
                    children: [{
                        value: 'government',
                        label: '政府机构'
                    }, {
                        value: 'scientific',
                        label: '科研教育'

                    }, {
                        value: 'medical',
                        label: '医疗服务'

                    }, {
                        value: 'park',
                        label: '公园广场'

                    }, {
                        value: 'commerce',
                        label: '商业大厦'

                    }, {
                        value: 'retail',
                        label: '零售行业'

                    }, {
                        value: 'food',
                        label: '餐饮服务'

                    }]
                }],
                default: ['facilityType', 'government'],
                selected: ['facilityType', 'government']
            },
            'o10': {
                'option': [{
                    value: 'plotStatisPOILyr',
                    label: '地块中心点',
                    children: [{
                        value: 'walk',
                        label: '步行',
                        children: [{
                            value: '0',
                            label: '10分钟'
                        }, {
                            value: '1',
                            label: '15分钟'
                        }, {
                            value: '2',
                            label: '20分钟'
                        }, {
                            value: '3',
                            label: '25分钟'
                        }, {
                            value: '4',
                            label: '30分钟'
                        }]
                    }, {
                        value: 'bike',
                        label: '骑行',
                        children: [{
                            value: '0',
                            label: '10分钟'
                        }, {
                            value: '1',
                            label: '15分钟'
                        }, {
                            value: '2',
                            label: '20分钟'
                        }, {
                            value: '3',
                            label: '25分钟'
                        }, {
                            value: '4',
                            label: '30分钟'
                        }]

                    }, {
                        value: 'bus',
                        label: '公共交通',
                        children: [{
                            value: '1',
                            label: '15分钟'
                        }, {
                            value: '2',
                            label: '20分钟'
                        }, {
                            value: '3',
                            label: '30分钟'
                        }]

                    }, {
                        value: 'taxi',
                        label: '驾车',
                        children: [{
                            value: '1',
                            label: '15分钟'
                        }, {
                            value: '2',
                            label: '20分钟'
                        }, {
                            value: '3',
                            label: '30分钟'
                        }]

                    }]
                }],
                default: ['plotStatisPOILyr', 'walk', '0'],
                selected: ['plotStatisPOILyr', 'walk', '0']
            },
            'o11': {
                'option': [{
                    value: 'action',
                    label: '出行模式',
                    children: [{
                            value: 'o',
                            label: '出发'
                        },
                        {
                            value: 'd',
                            label: '到达'
                        },
                        {
                            value: 'od',
                            label: '路径'
                        },
                        {
                            value: 'track',
                            label: '轨迹',
                            disabled: true
                        }
                    ],
                }],
                default: ['action', 'o'],
                selected: ['action', 'o']
            },
            'o12': {
                'option': [{
                    value: 'xzList',
                    label: '乡镇',
                    // children: [{
                    //         value: 'lucheng',
                    //         label: '潞城镇'
                    //     },
                    //     {
                    //         value: 'guoxian',
                    //         label: '漷县镇'
                    //     },
                    //     {
                    //         value: 'songzhuang',
                    //         label: '宋庄镇'
                    //     },
                    //     {
                    //         value: 'xiji',
                    //         label: '西集镇'
                    //     },
                    //     {
                    //         value: 'taihu',
                    //         label: '台湖镇'
                    //     },
                    //     {
                    //         value: 'xinhua',
                    //         label: '新华街道'
                    //     },
                    //     {
                    //         value: 'yongshun',
                    //         label: '永顺地区'
                    //     },
                    //     {
                    //         value: 'liyuan',
                    //         label: '梨园地区'
                    //     },
                    //     {
                    //         value: 'zhongcang',
                    //         label: '中仓街道'
                    //     },
                    //     {
                    //         value: 'beiyuan',
                    //         label: '北苑街道'
                    //     },
                    //     {
                    //         value: 'yuqiao',
                    //         label: '玉桥街道'
                    //     },
                    //     {
                    //         value: 'maju',
                    //         label: '马驹桥镇'
                    //     },
                    //     {
                    //         value: 'zhangjiawan',
                    //         label: '张家湾镇'
                    //     },
                    //     {
                    //         value: 'yongle',
                    //         label: '永乐店镇'
                    //     },
                    //     {
                    //         value: 'yujiawu',
                    //         label: '于家务回族乡'
                    //     }
                    // ],
                    children: [{
                            value: '110112119000',
                            label: '潞城镇'
                        },
                        {
                            value: '110112106000',
                            label: '漷县镇'
                        },
                        {
                            value: '110112104000',
                            label: '宋庄镇'
                        },
                        {
                            value: '110112110000',
                            label: '西集镇'
                        },
                        {
                            value: '110112114000',
                            label: '台湖镇'
                        },
                        {
                            value: '110112002000',
                            label: '新华街道'
                        },
                        {
                            value: '110112005000',
                            label: '永顺地区'
                        },
                        {
                            value: '110112006000',
                            label: '梨园地区'
                        },
                        {
                            value: '110112001000',
                            label: '中仓街道'
                        },
                        {
                            value: '110112003000',
                            label: '北苑街道'
                        },
                        {
                            value: '110112004000',
                            label: '玉桥街道'
                        },
                        {
                            value: '110112109000',
                            label: '马驹桥镇'
                        },
                        {
                            value: '110112105000',
                            label: '张家湾镇'
                        },
                        {
                            value: '110112117000',
                            label: '永乐店镇'
                        },
                        {
                            value: '110112209000',
                            label: '于家务回族乡'
                        }
                    ],
                }],
                default: ['xzList', '110112119000'],
                selected: ['xzList', '110112119000']
            },

        },
        // 存值的：原本是数组，改为对象，为了保留默认的一级（选中的）
        selectOptions: {},
        //判断：是否变化的 
        selectActive: []
    }

}