//日期格式转换
export let dateFormatChange = (val) => {
    if (val != null) {
        var datetime = new Date(val);


        var year = datetime.getFullYear();
        var month = datetime.getMonth() + 1; //js从0开始取 
        var date = datetime.getDate();
        // var hour = datetime.getHours();
        // var minutes = datetime.getMinutes();
        // var second = datetime.getSeconds();

        if (month < 10) {
            month = "0" + month;
        }
        if (date < 10) {
            date = "0" + date;
        }
        // if (hour < 10) {
        //     hour = "0" + hour;
        // }
        // if (minutes < 10) {
        //     minutes = "0" + minutes;
        // }
        // if (second < 10) {
        //     second = "0" + second;
        // }

        var time = year + "-" + month + "-" + date;

        return time;
    }
    return "";
}