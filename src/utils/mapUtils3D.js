/* eslint-disable no-undef */
// var map={},heatlayer={},polygonColors= {},ODLayer= {},traceData= {},dataHeat= {};
import store from '../store/index'

var colorsRender = [
    '#00ffbf',
    '#167cff',
    '#4cff00',
    '#ff5f00',
    '#40ffcf',
    '#3c75bf',
    '#5bbf30',
    '#bf6530',
    '#73ffdc',
    '#074ca6',
    '#31a600',
    '#a63e00'
];
export default {
    initMap(paramCenter) {
        var map = new AMap.Map("container", {
            mapStyle: "amap://styles/dark",
            zoom: paramCenter.zoomLev,
            center: paramCenter.centerPt
        });
        //   AMapUI.loadUI(['control/BasicControl'], function(BasicControl) {
        //     //添加一个缩放控件
        //     map.addControl(new BasicControl.Zoom({
        //         position: 'rb'
        //     }));
        // });
        return map;
    },

    //设置图层级别可见，在map的zoomend事件中使用
    setZooms(map, Layer, level1, level2) {
        var zoom = map.getZoom();

        if (zoom > level1 && zoom < level2) {
            Layer.show();
        } else {
            Layer.hide();
        }
    },
    //地图缩放、获取当前等级
    zoomIn(map) { //放大
        map.zoomIn();
    },
    zoomOut(map) { //缩小
        map.zoomOut();
    },
    getZoomLevel(map) { //获取当前缩放级别
        return map.getZoom();
    },

    //地图重绘，保留底图，移除其他图层
    reDrawMap(map, heatlayer, polygonColors, MarkerClusterer) {
        // debugger
        const arrLayer = map.getLayers();
        const cLayer = arrLayer[arrLayer.length - 1];
        if (cLayer.CLASS_NAME.indexOf("Custom") != -1) {
            map.remove(cLayer);
        }
        if (heatlayer) {
            map.remove(heatlayer);
        }
        if (polygonColors) {
            map.remove(polygonColors);
        }
        if (MarkerClusterer) {
            map.remove(MarkerClusterer);
        }
    },
    //添加边界数据
    addPolygonLayer(map, GeoJson2, polygonColors) {
        if (polygonColors) {
            map.remove(polygonColors);
        }
        var geojson = new AMap.GeoJSON({
            geoJSON: GeoJson2,
            // 还可以自定义getMarker和getPolyline
            getPolygon: function(GeoJson2, lnglats) {
                var color2 = "white";

                return new AMap.Polygon({
                    path: lnglats,
                    strokeColor: "white",
                    strokeOpacity: 0.5,
                    fillOpacity: 0.05,
                    fillColor: color2
                });
            }
        });
        map.setZoomAndCenter(GeoJson2.mapOption.zoom, GeoJson2.mapOption.center);
        geojson.setMap(map);
        // return geojson;
    },
    //填色图
    addColorPolygon(map, geoJSON) {
        //const _this = this;
        var geojson = new AMap.GeoJSON({
            geoJSON: geoJSON,
            // 还可以自定义getMarker和getPolyline
            getPolygon: function(geojson, lnglats) {
                var color2 = "red";
                if (geojson.properties._parentProperities.count) {
                    var countC = geojson.properties._parentProperities.count;
                    if (countC < 3) {
                        color2 = colorsRender[0];
                    } else if (countC >= 3 && countC < 5) {
                        color2 = colorsRender[1];
                    } else if (countC >= 5 && countC < 10) {
                        color2 = colorsRender[2];
                    } else if (countC >= 10 && countC < 20) {
                        color2 = colorsRender[3];
                    } else if (countC >= 20) {
                        color2 = colorsRender[4];
                    }
                }
                return new AMap.Polygon({
                    path: lnglats,
                    strokeColor: "white",
                    strokeOpacity: 0.2,
                    fillOpacity: 0.2,
                    fillColor: color2
                });
            }
        });

        //var cThis=this;
        //this.$store.commit('changeMapClickPolyName', 'clickName');

        geojson.on("click", function(e) {
            var obj = e.target;
            var prop = obj.getExtData();
            var polygonCode;
            // if(flag==0){
            // polygonCode= prop._geoJsonProperties._parentProperities.o_xz_code;
            // }else{
            polygonCode = prop._geoJsonProperties._parentProperities.xz_code;
            // }
            store.dispatch('map/updatepolycode', polygonCode);
        });
        geojson.setMap(map);
        map.on('zoomend', function() {
            this.setZooms(map, geojson, 10, 14);
        });
        return geojson;
    }
}