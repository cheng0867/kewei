import Vue from 'vue'
import Router from 'vue-router'

import home from '@/pages/home.vue';
// 测试数据
// import mapTestRouter from './mapTest';
// 城市行为认知子系统
// 个体行为模型
import region from '@/pages/cityPlan/region.vue'
import population from '@/pages/cityPlan/population.vue'
import occupation from '@/pages/cityPlan/occupation.vue'
import traffic from '@/pages/cityPlan/traffic.vue'
import block from '@/pages/cityPlan/block.vue'
import individualBehavior from '@/pages/cityAction/individualBehavior.vue'
import populationDynamics from '@/pages/cityAction/populationDynamics.vue'
import horizontalData from '@/pages/cityAction/horizontalData.vue'
import directionData from '@/pages/cityAction/directionData.vue'



Vue.use(Router)



export default new Router({
    mode: 'history',
    routes: [{
            path: '/home',
            component: home
        },
        {
            path: '/region',
            component: region
        },
        {
            path: '/population',
            component: population
        },
        {
            path: '/occupation',
            component: occupation
        },
        {
            path: '/traffic',
            component: traffic
        },
        {
            path: '/block',
            component: block
        },
        {
            path: '/individualBehavior',
            component: individualBehavior
        },
        {
            path: '/populationDynamics',
            component: populationDynamics
        },
        {
            path: '/horizontalData',
            component: horizontalData
        },
        {
            path: '/directionData',
            component: directionData
        },
        // mapTestRouter,
        {
            path: '/',
            redirect: '/home'
        }

    ]
})